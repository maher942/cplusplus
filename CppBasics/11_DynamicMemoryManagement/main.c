#include <iostream>
#include <new>

using namespace std;

int main ()
{
  int i,n;
  int* _pointer;
  cout << "How many numbers would you like to type? ";
  cin >> i;
 _pointer= new (nothrow) int[i];
  if (_pointer == nullptr)
    cout << "Error: memory could not be allocated";
  else
  {
    for (n=0; n<i; n++)
    {
      cout << "Enter number: ";
      cin >> _pointer[n];
    }
    cout << "You have entered: ";
    for (n=0; n<i; n++)
      cout << _pointer[n] << ", ";
    delete[] _pointer;
  }
  return 0;
}