# Main Makefile

# Find all directories containing a Makefile, excluding the current directory (./)
SUBDIRS := $(shell find . -mindepth 2 -type f -name 'Makefile' -exec dirname {} \;)

# Target to run 'make' in each subdirectory
.PHONY: all $(SUBDIRS)

all: $(SUBDIRS)

# Loop through each subdirectory and run 'make'
$(SUBDIRS):
	@echo "Running make in $@"
	$(MAKE) -C $@

# Optionally, define clean to clean all subdirectory builds
clean:
	@for dir in $(SUBDIRS); do \
		echo "Cleaning in $$dir"; \
		$(MAKE) -C $$dir clean; \
	done

