UNAME := $(shell uname)
current_dir :=$(shell pwd)

ifndef TARGET
TARGET = LINUX-X86
endif

ifndef CONF
CONF=Release
endif

ifeq ($(CONF), DEBUG)
CXXFLAGS += -O0 -g3 -Wall 
else ifeq ($(CONF), Release)
CXXFLAGS += -O3 -Wall 
endif


ifeq ($(CONF), DEBUG)
TARGET_DIR = $(current_dir)/Cible/Linux_x86/Debug_x86
else ifeq ($(CONF), Release)
TARGET_DIR = $(current_dir)/Cible/Linux_x86/Release_X86
endif





