#ifndef _SINGLETON_H_
#define _SINGLETON_H_


#include<iostream>

class Singleton
{
private:
	/* Here will be the instance stored. */
	static Singleton * instance;
	
	/* Private constructor to prevent instancing. */
	Singleton();
	
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;

public:

	/* Static access method. */
	static Singleton* getInstance();
	~Singleton();
};
#endif
