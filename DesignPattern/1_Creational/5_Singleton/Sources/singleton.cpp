#include "singleton.h"


/* Null, because instance will be initialized on demand. */
Singleton* Singleton::instance = 0;


Singleton::Singleton()
{
	std::cout << "Constructor Singleton" << std::endl;
}

Singleton* Singleton::getInstance()
{
	if(instance == 0)
		instance = new Singleton();
	return instance;
}

Singleton::~Singleton()
{
	std::cout << "Destructor Singleton" << std::endl;
}

