#include "singleton.h"

int main()
{
	std::cout << "Starting Main" << std::endl;

	//new Singleton(); // Won't work
    Singleton* s = Singleton::getInstance(); // Ok
    Singleton* r = Singleton::getInstance();

    /* The addresses will be the same. */
    std::cout << s << std::endl;
    std::cout << r << std::endl;

	std::cout << "Terminating Main" << std::endl;
}
