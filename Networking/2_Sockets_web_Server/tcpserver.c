/*
*@ Author : HAMMAMI Maher
*/

#include "tcpserver.h"

//binTohex function
char *binTohex(const unsigned char *i_ucinput, size_t len)
{
	char *l_pcResult;
	char *l_pcHexits = "0123456789ABCDEF";
	
	if(i_ucinput == NULL || len <= 0)
		return NULL;
	
	int l_iResultlength = (len*3) +1;
	l_pcResult = malloc(l_iResultlength);
	bzero(l_pcResult, l_iResultlength);
	
	for(int i=0; i<len; i++)
	{
		l_pcResult[i*3] = l_pcHexits[i_ucinput[i]>>4];
		l_pcResult[(i*3) +1] = l_pcHexits[i_ucinput[i] & 0x0F];
		l_pcResult[(i*3) +2] = ' '; 
	}
	return l_pcResult;
}

int main(int argc, char** argv)
{
	int 			listenfd, connfd, n;
	struct 		sockaddr_in servaddr;
	
	uint8_t 	buff[MAXLINE+1];
	uint8_t 	recvline[MAXLINE+1];
	
	
	if(argc != 2)
  {
    printf("Usage : %s <server address>\n", argv[0]);
    printf("Example : %s 31.13.69.35\n", argv[0]);
    exit(1);
  }
  
  
	if((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printf("Error while creating the socket!\n");
    exit(1);
	}
	
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family					= AF_INET;
	servaddr.sin_port						= htons(SERVER_PORT); 
	
	if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)      //translate the address 1.2.3.4 ==>[1,2,3,4]
  {
    printf("inet_pton error for %s \n", argv[1]);
    exit(1);
  } 
	
	
	//bind and listen
	if((bind(listenfd, (SA*) &servaddr, sizeof(servaddr))) < 0)
	{
		printf("Bind Error !\n");
		exit(1);
	}
	
	if((listen(listenfd,10)) < 0)
	{
		printf("listen error!\n");
		exit(1);
	}
	
	//loop
	
	for(; ;)
	{
		struct sockaddr_in addr;
		socklen_t addr_len;
		
		//accept bloks until an uncomig connection arrives
		//it returns a file descriptor to the connection
		printf("waiting for connection on port %d \n", SERVER_PORT);
		//printf("waiting for connection on port %d \n", argv[2]);
		fflush(stdout);
		connfd = accept(listenfd, (SA*) NULL, NULL);
		
		//zero out the receive buffer to make sure it ends up null terminated
		memset(recvline, 0, MAXLINE);
		
		//Now read the client message
		while((n = read(connfd, recvline, MAXLINE-1))> 0)
		{
			fprintf(stdout, "\n%s\n\n%s \n", binTohex(recvline, n),recvline);
			
			//hacky way to detect the end of the message
			if(recvline[n-1] == '\n')
			{
				break;
			}
			memset(recvline, 0, MAXLINE);
		}
		if(n <0)
		{
			printf("read error !\n");
			exit(1);
		}
		
		//now send a response to the client
		snprintf((char*)buff, sizeof(buff), "HTTP/1.0 200 OK\r\n\r\nMourad YA mezyen w tahan fared wa9et");
		
		//if you want check the result from close and write
		//in case errors occur, for noz, i'm ignoring them
		write(connfd, (char*)buff, strlen((char*)buff));
		close(connfd);	
	}
	exit(0);
}
