/*
*@ Author : HAMMAMI Maher
*/

#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>


#define SERVER_PORT 8082

#define MAXLINE     100      //buffer length where data goes

#define SA struct sockaddr    //less wordy types  

char *binTohex(const unsigned char *i_ucinput, size_t len);

#endif
