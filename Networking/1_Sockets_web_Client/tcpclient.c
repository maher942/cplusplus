/*
*@ Author : HAMMAMI Maher
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

//standard HTTP port
#define SERVER_PORT 8082        //server port where we're connecting

#define MAXLINE     4096      //buffer length where data goes

#define SA struct sockaddr    //less wordy types               
int main(int argc, char**argv)
{
  int sockfd, n;
  int sendbytes;
  
  struct sockaddr_in servaddr;
  
  char sendline[MAXLINE];
  char recvline[MAXLINE];
  
  
  
  if(argc != 2)
  {
    printf("Usage : %s <server address>\n", argv[0]);
    printf("Example : %s 31.13.69.35n", argv[0]);
    exit(1);
  }
  //create a socket
  //AF_NET : Address Family - internet
  //SOCK_STREAM : Stream Socket
  //0 == use TCP (default)
  sockfd = socket(AF_INET, SOCK_STREAM,0);
  if(sockfd < 0)
  {
    printf("Error while creating the socket!\n");
    exit(1);
  }
  
  bzero(&servaddr, sizeof(servaddr));       //zero out the address
  servaddr.sin_family = AF_INET;            //AF_INET
  servaddr.sin_port = htons(SERVER_PORT);   //PORT, htons == "host to network, short"
  
  if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)      //translate the address 1.2.3.4 ==>[1,2,3,4]
  {
    printf("inet_pton error for %s \n", argv[1]);
    exit(1);
  }
  
  if(connect(sockfd, (SA*) &servaddr, sizeof(servaddr)) < 0)
  {
    printf("connect failed!\n");
    exit(1);
  }
  
  //we're connected, prepare the message
  
  sprintf(sendline, "GET / HTTP/1.0 hello from client\r\n\r\n");
  sendbytes = strlen(sendline);
  
  //send the request -- making sure you send all the data
  if(write(sockfd, sendline, sendbytes) != sendbytes)
  {
    printf("write error!\n");
    exit(1);
  } 
  
  memset(recvline, 0, MAXLINE);
  
  //Now read the server's response
  if((n = read(sockfd, recvline, MAXLINE-1)) > 0)
  {
    printf("%s", recvline);
  }
  if(n < 0)
  {
    printf("read error!\n");
    exit(1);
  }
  exit(0); //end seccessfully
}
